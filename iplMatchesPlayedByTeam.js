const fs = require('fs');
const csv = require('csv-parser');
(()=>{
let matchesCountPerSeason = {};
fs.createReadStream('matches.csv').pipe(csv())
    .on('data', (match) => {
       
        currSeason = match["season"];
        if (!matchesCountPerSeason[currSeason]) {
            matchesCountPerSeason[currSeason] = 1;
        } else {
            matchesCountPerSeason[currSeason]++;
        }
        
    })

    .on('end', () => {
        console.log('CSV file successfully processed');
        console.log(matchesCountPerSeason);
       
    })
})()