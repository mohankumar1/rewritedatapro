const fs = require('fs');
const csv = require('csv-parser');
(() => {
    const matchesWonOfPerTeamPerYear = {}
    fs.createReadStream('matches.csv').pipe(csv())
        .on('data', (match) => {

            currSeason = match["season"];
            currWinner = match["winner"];

            if (!matchesWonOfPerTeamPerYear[currSeason]) {

                matchesWonOfPerTeamPerYear[currSeason] = {}
                matchesWonOfPerTeamPerYear[currSeason][currWinner] = 1
            } else if (!matchesWonOfPerTeamPerYear[currSeason][currWinner]) {

                matchesWonOfPerTeamPerYear[currSeason][currWinner] = 1
            } else {

                matchesWonOfPerTeamPerYear[currSeason][currWinner] += 1
            }
        })
        .on('end', () => {

            console.log('CSV file successfully processed');
            console.log(matchesWonOfPerTeamPerYear)
        })
})()